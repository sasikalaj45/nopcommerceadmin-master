package nopcommerceadmin;

import nopcommerceadmin.Utils.Data_From_Excel;
import org.openqa.selenium.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;
import static org.testng.Assert.assertEquals;

/*
 * Created by Sasikala on 05/03/2015.

   Test suite for manage products functions.
   The script is designed using Hybrid Framework employing Function Driven , Data Driven , Page
   Object Model and Test Object Model
   Suite include tests for important features such as
   1) Add products in bulk to the database using data driven framework. Data fed from Excel.
   2) Add single product with data from excel
   3) Edit a product
   4) Edit a product attribute Search Engine Optimisation(SEO)
   5) Delete a product

   TestNG XML is used to maintain the dependencies and to preserve the order of execution mentioned above

   --> Selenium APIs used in the scripts includes Select Class , Actions Class ,
       WebDriverWait,Expected Conditions
   --> Also java.awt.ROBOT APIs are used to interact with windows control
   --> Third Party Apache POI APIs are used to read data from Excel
*/
public class Manage_Product_Test_Suite extends Base_Test_Suite {

/*
    Create Page Object instances required for the suite.
*/

    public Home_Page   home_page                            = new Home_Page();
    public Manage_Product_Page manage_product_page          = new Manage_Product_Page();
    public Add_Product_Page add_product_page                = new Add_Product_Page();
    public Edit_Products_Page edit_products_page            = new Edit_Products_Page();
    public Import_From_Excel_Page import_from_excel_page    = new Import_From_Excel_Page();
    public Product_SEO_Page seo_page                        = new Product_SEO_Page();

/*
    Test data required for the suite
*/
    private String edit_sku             = "943534-01-2";
    private String SEO_update_string    = "ABCD";
    private String product_name         = "Canon Digital SLR Camera - Auburn";
    private String admin_data           = "\"You are top in m wishlist. I will buy you\"";
    private String [][] product_data ;
    private int    no_of_rows = 5 , no_of_columns = 80;
    private String file_path_for_bulk   = "C:\\Users\\Sasikala\\Downloads\\bulk_products.xlsx";
    private String file_path_a_product  = "C:\\Users\\Sasikala\\Downloads\\a_product.xlsx";
    private String upload_file          = "C:\\Users\\Sasikala\\Downloads\\products.xlsx";

/*
   * Automation test script using Data Driven Framework to insert multiple products
   * Products' data is obtained by "export to excel" option from the nop-commerce demo site
   * Product ids were modified and used as input to this script
   * Products are added iteratively one by one from the excel.
   * Around 17 fields per products are populated using the below script.
   * Script uses third party APIs APACHE excel reader, selenium "Actions", "Select" APIs.
*/

    @Test(dataProvider = "product_Data")

    public void add_Bulk_Products_From_Excel(String... excel_data) throws Exception {

        home_page.goto_Manage_Product_Page();

        manage_product_page.goto_Add_NewProduct_Page();

        add_product_page.add_New_Product(excel_data);

        assertEquals("The new product has been added successfully.", driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div[1]")).getText());
    }

    @DataProvider(name = "product_Data")
    public String[][] product_Data() throws IOException {
        return Data_From_Excel.excel_Data(file_path_for_bulk, no_of_rows,no_of_columns);
    }
/*
    Test to add a product. Data fed from excel
*/
    @Test
    public void add_A_Product() throws Exception {

        product_data = Data_From_Excel.excel_Data(file_path_a_product, 1 ,no_of_columns);

        home_page.goto_Manage_Product_Page();

        manage_product_page.goto_Add_NewProduct_Page();

        add_product_page.add_New_Product(product_data[0]);

        assertEquals("The new product has been added successfully.", driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div[1]")).getText());
    }
/*
    Test to edit the admin content of a product.
*/
    @Test
    public void edit_A_Product() throws Exception {

        home_page.goto_Manage_Product_Page();

        manage_product_page.search_product(product_name);

        manage_product_page.is_product_present(product_name);

        manage_product_page.select_a_product_to_edit(product_name);

        edit_products_page.edit_Admin_Content(admin_data);

        edit_products_page.save_edit_product();

        assertEquals("The product has been updated successfully.", driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div[1]")).getText());

    }
/*
    Test to edit Search Engine Optimisation(SEO) metadata of a product.
*/
    @Test
    public void edit_Product_SEO() throws Exception {

        home_page.goto_Manage_Product_Page();

        manage_product_page.goto_Sku_Page(edit_sku);

        edit_products_page.goto_SEO_Page();

        seo_page.update_Product_Meta_Keywords(SEO_update_string);

        assertEquals(driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div[1]")).getText(), "The product has been updated successfully.");

    }
/*
    Test to import list of products from excel. Script uses the ROBOT APIs from java.awt package
*/
    @Test
    public void import_Product_From_Excel() throws AWTException
    {

        home_page.goto_Manage_Product_Page();

        manage_product_page.goto_Import_from_excel_page();

        import_from_excel_page.import_from_excel_dialog(upload_file);

        assertEquals("For security purposes, the feature you have requested is not available on the demo site.",driver.findElement(By.xpath("html/body/div[2]/div/div[5]/div[1]")).getText());

    }

/*
    Test to delete a product.
*/
    @Test public void delete_Product() throws Exception {

        home_page.goto_Manage_Product_Page();

        manage_product_page.search_product(product_name);

        manage_product_page.is_product_present(product_name);

        manage_product_page.select_a_product(product_name);

        manage_product_page.delete_a_product();

        manage_product_page.search_product(product_name);

        assertEquals("No items to display",driver.findElement(By.xpath("//*[@id='products-grid']/div/span[2]")).getText());
    }
}
