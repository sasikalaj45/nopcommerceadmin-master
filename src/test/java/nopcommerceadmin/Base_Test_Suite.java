package nopcommerceadmin;

import org.openqa.selenium.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.fail;
import nopcommerceadmin.Utils.*;
/**
 * Created by Sasikala on 05/03/2015.
 * Abstract Base test provides Webdriver instance,basic set up and wrap up functions for other test suites.
 */

public abstract class Base_Test_Suite {

    public WebDriver        driver;
    private String          baseUrl = "https://admin-demo.nopcommerce.com/";
    private StringBuffer    verificationErrors = new StringBuffer();
    public Login_Page       login_page           = new Login_Page();
    private String          email_id= "admin@yourStore.com";
    private String          password="admin" ;

    @BeforeClass
    public void setup_Test()
    {
        driver = Browser_Factory.getBrowser("firefox");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl + "/");
        login_page.login(email_id, password);
    }

    @AfterClass
    public void tear_Down_After_Test() throws Exception
    {
            driver.quit();
            String verificationErrorString = verificationErrors.toString();
            if (!"".equals(verificationErrorString)) {
                fail(verificationErrorString);
            }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
    }
}

