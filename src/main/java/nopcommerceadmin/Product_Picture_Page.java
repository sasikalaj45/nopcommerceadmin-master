package nopcommerceadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/**
 * Created by Sasikala on 05/03/2015.
 * provides functions to add product images
 */

public class Product_Picture_Page extends Edit_Products_Page {

    public void upload_product_picture() throws Exception {

        Robot robot = new Robot();
        WebElement uploadfile = driver.findElement(By.xpath("//div[contains(text(),'Upload a file')]/following-sibling::input"));

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",uploadfile);

        String string = "\" C:\\Users\\Sasikala\\Documents\\Jobs\\Others\\download.jpg\"";

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.delay(10000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        robot.delay(1000);

        driver.findElement(By.id("addProductPicture")).click();

        robot.delay(1000);
        driver.findElement(By.xpath(".//*[@id='productpictures-grid']/table/tbody/tr/td[1]/a[1]/img"));

    }
}
