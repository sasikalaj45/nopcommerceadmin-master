package nopcommerceadmin;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import org.openqa.selenium.*;

/**
 * Created by Sasikala on 05/03/2015.
 * Use the java.awt.Robot api to control the windows functions to import the prodcut details from xls
 */

public class Import_From_Excel_Page extends Manage_Product_Page{

    public void import_from_excel_dialog(String upload_file) throws AWTException
    {
    Robot robot = new Robot();

    driver.findElement(By.id("importexcelfile")).click();

    StringSelection stringSelection = new StringSelection(upload_file);
    Toolkit.getDefaultToolkit().
    getSystemClipboard().setContents(stringSelection, null);

    robot.delay(10000);
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_V);
    robot.keyRelease(KeyEvent.VK_V);
    robot.keyRelease(KeyEvent.VK_CONTROL);
    robot.delay(10000);
    robot.keyPress(KeyEvent.VK_ENTER);
    robot.keyRelease(KeyEvent.VK_ENTER);

    robot.delay(1000);

    driver.findElement(By.xpath("//*[@id='importexcel-window']/form/table/tbody/tr[3]/td/input")).click();
}
}
