package nopcommerceadmin.Utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Sasikala on 05/03/2015.
 */
public class Alert_Utils {

    private static WebDriver driver = Browser_Factory.getBrowser("firefox");
    private static boolean accept_Next_Alert = true;

    public static boolean is_Alert_Present() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.alertIsPresent());
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
        catch (TimeoutException e ){
            return false;
        }
    }

    public static String close_Alert_And_Get_Its_Text() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (accept_Next_Alert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            driver.switchTo().defaultContent();
            return alertText;
        } finally {
            accept_Next_Alert = true;
        }
    }

}
