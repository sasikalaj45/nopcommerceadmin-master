package nopcommerceadmin.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Created by Sasikala on 05/03/2015.
 */

public class Browser_Factory {

    private static WebDriver mBrowser;

    public static WebDriver getBrowser(String browser) {

        if (mBrowser == null)
        {
            if (browser.equalsIgnoreCase("firefox")) {
                mBrowser = new FirefoxDriver();
            } else if (browser.equalsIgnoreCase("ie")) {
                mBrowser = new InternetExplorerDriver();
            } else if (browser.equalsIgnoreCase("chrome")) {
                mBrowser = new ChromeDriver();
            } else if (browser.equalsIgnoreCase("safari")) {
              mBrowser = new SafariDriver();
            } else {
            // Default browser is set to firefox browser
            mBrowser = new FirefoxDriver();
            }
        }
        return mBrowser;
    }
}
