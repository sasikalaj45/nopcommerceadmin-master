package nopcommerceadmin.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sasikala on 05/03/2015.
 */
public class Wait_Utils {

    private static WebDriver driver = Browser_Factory.getBrowser("firefox");
    private static WebDriverWait wait = new WebDriverWait(driver, 10);


    public static boolean is_Text_Available(String text)
    {
        return driver.getPageSource().contains(text);
    }

    public static void pause(int time)
    {
        try {
          Thread.sleep(time*1000);
         } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void wait_For_Visibility_Of_Element( WebElement element)
    {
    wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void wait_For_Element_Displayed(By by)
    {
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void implicitWait()
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public static boolean is_Element_Present(WebElement element) {
        try {
            implicitWait();
            element.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean is_Element_Present(By by) {
        try {
            implicitWait();
            driver.findElement(by).isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
     }
}
