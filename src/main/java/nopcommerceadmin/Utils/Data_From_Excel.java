package nopcommerceadmin.Utils;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;

/**
 * Created by Sasikala on 05/03/2015.
 */
public class Data_From_Excel {

    public static String [][] excel_Data(String file_path, int no_of_rows , int no_of_columns ) throws IOException {

        int i, j;
        i = j = 0;
        String[][] excelData = new String[no_of_rows][no_of_columns];
        FileInputStream file = new FileInputStream(new File(file_path));

        Workbook workbook = new XSSFWorkbook(file);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        while (rowIterator.hasNext() & i < no_of_rows) {

            j=0;
            Row row = rowIterator.next();

            //For each row, iterate through each columns
            Iterator<Cell> cellIterator = row.cellIterator();

            while (cellIterator.hasNext()& j< no_of_columns) {

                Cell cell = cellIterator.next();

                switch (cell.getCellType()) {

                    case Cell.CELL_TYPE_STRING:
                        excelData[i][j] = cell.getStringCellValue();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        excelData[i][j] = new BigDecimal(cell.getNumericCellValue()).toPlainString();
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        excelData[i][j] = new Boolean(cell.getBooleanCellValue()).toString();
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        excelData[i][j] = " ";
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        switch (cell.getCachedFormulaResultType()){
                            case Cell.CELL_TYPE_STRING:
                                excelData[i][j] = cell.getStringCellValue();
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                excelData[i][j] = cell.getNumericCellValue()+"";
                                break;
                            default:
                        }
                        excelData[i][j] = " ";
                        break;
                    default:
                        break;
                }
                j++;

            } //end of cell iterator
            i++;
        }//end of row iterator
        file.close();
        return excelData;
    }
}
