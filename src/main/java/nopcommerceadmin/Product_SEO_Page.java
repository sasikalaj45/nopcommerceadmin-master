package nopcommerceadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Sasikala on 05/03/2015.
 * Edit Product Search Engine Optimisation meta data
 */
public class Product_SEO_Page extends Edit_Products_Page{
    public void update_Product_Meta_Keywords(String SEO_update_string)
    {
       WebElement meta_keywords = driver.findElement(By.xpath("//*[@id='MetaKeywords']"));
       meta_keywords.clear();
       meta_keywords.sendKeys(SEO_update_string);
       driver.findElement(By.xpath("//*[@id='product-form']/div[1]/div[2]/input[1]")).click();
    }
}


