package nopcommerceadmin;


import nopcommerceadmin.Utils.*;
import org.openqa.selenium.WebDriver;

/**
 * Created by Sasikala on 05/03/2015.
 * This is an abstract class for providing WebDriver instance for the rest of the Page objects
 */

public abstract class Abstract_Page {

    public static WebDriver driver;

    public Abstract_Page()
    {
        driver = Browser_Factory.getBrowser("firefox");
    }
}
