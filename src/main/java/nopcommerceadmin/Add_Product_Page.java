package nopcommerceadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by Sasikala on 05/03/2015.
 * Contains functions which populates data in the product page and add the product to the database
 */
public class Add_Product_Page extends Home_Page {

    public void add_New_Product(String ... excel_data)
    {
        add_Admin_Content(excel_data);
        add_Product_Data(excel_data);
        add_Product();
    }

    private void add_Admin_Content(String... data) {
        // if the product id is 10 ,then it is grouped product , but in the screen the default is simple product, value 5. So, check the product id from excel and reset it accordingly

        if (data[0].equals("10")) {
            new Select(driver.findElement(By.id("ProductTemplateId"))).selectByVisibleText("Simple product");
        } else {
            new Select(driver.findElement(By.id("ProductTemplateId"))).selectByVisibleText("Grouped product");
        }
        // Based on the data from the excel apply the visibility factor for the product.
        //if true click the radio button if not(!) already selected

        if (data[2].equals("True")) {
            // if not selected, select
            if (!driver.findElement(By.id("VisibleIndividually")).isSelected()) {
                driver.findElement(By.id("VisibleIndividually")).click();
            }
        } else {
            // if selected enabled, disable it
            if (driver.findElement(By.id("VisibleIndividually")).isSelected()) {
                driver.findElement(By.id("VisibleIndividually")).click();
            }
        }

        driver.findElement(By.xpath(".//*[@id='Name']")).sendKeys(data[3]);

        driver.findElement(By.id("ShortDescription")).sendKeys(data[4]);

        // Switch to iframe to populate "full-description" of product

        driver.switchTo().frame(0);
        WebElement welem = driver.findElement(By.xpath(".//*[@id='tinymce']"));
        welem.click();
        welem.sendKeys(data[5]);
        driver.switchTo().defaultContent();

        driver.findElement(By.id("AdminComment")).sendKeys("No Full description available yet");

        //Based on the excel data set the Vendor details
        if (data[6].equals("0")) {
            new Select(driver.findElement(By.id("VendorId"))).selectByVisibleText("No vendor");
        }

        // Based on the data in the excel enable the ShowOnPage button
        if (data[8].equals("TRUE")) {
            if (!driver.findElement(By.id("ShowOnHomePage")).isSelected()) {
                driver.findElement(By.id("ShowOnHomePage")).click();
            }
        } else {
            if (driver.findElement(By.id("ShowOnHomePage")).isSelected()) {
                driver.findElement(By.id("ShowOnHomePage")).click();
            }
        }
        // Based on the data in the excel enable the AllowCustomerReviews button
        if (data[13].equals("TRUE")) {
            if (!driver.findElement(By.id("AllowCustomerReviews")).isSelected()) {
                driver.findElement(By.id("AllowCustomerReviews")).click();
            }
        } else {
            if (driver.findElement(By.id("AllowCustomerReviews")).isSelected()) {
                driver.findElement(By.id("AllowCustomerReviews")).click();
            }
        }
        //Populate Sku
        driver.findElement(By.id("Sku")).sendKeys(data[15]);

        //Populate ManufacturerpartNumber
        if (data[16].length() > 0) {
            driver.findElement(By.id("ManufacturerPartNumber")).sendKeys(data[16]);
        } else {
            driver.findElement(By.id("ManufacturerPartNumber")).sendKeys("106754-A");
        }
        //Populate GTIN number

        if (data[17].length() > 0) {
            driver.findElement(By.id("Gtin")).sendKeys(data[17]);
        } else {
            driver.findElement(By.id("Gtin")).sendKeys("943534");
        }
    }

    private void add_Product_Data(String... data) {
        if (data[61].length() > 0) {

            driver.findElement(By.xpath(".//*[@id='group-prices']/tbody/tr[2]/td[2]/span[1]/span/input[1]")).sendKeys
                    (data[61]);
        } else {

            driver.findElement(By.xpath(".//*[@id='group-prices']/tbody/tr[2]/td[2]/span[1]/span/input[1]")).sendKeys
                    ("1.00");
        }


        if (data[64].length() > 0) {
            driver.findElement(By.xpath(".//*[@id='group-prices']/tbody/tr[5]/td[2]/span[1]/span/input[1]")).sendKeys(data[64]);

        } else {
            driver.findElement(By.xpath(".//*[@id='group-prices']/tbody/tr[5]/td[2]/span[1]/span/input[1]")).sendKeys
                    ("1.00");
        }

        driver.findElement(By.xpath(".//*[@id='group-prices']/tbody/tr[6]/td[2]/span[1]/span/span/span[1]")).click();

        driver.findElement(By.xpath("//*[@id='SpecialPriceStartDateTimeUtc_dateview']/div/div/a[3]/span")).click();

        driver.findElement(By.xpath("//*[@id='SpecialPriceStartDateTimeUtc_dateview']/div/table/tbody/tr[2]/td[4]/a")).click();

        driver.findElement(By.id("SpecialPriceEndDateTimeUtc")).sendKeys("19-Mar-2015");
    }

    private void add_Product() {
        driver.findElement(By.xpath(".//*[@id='product-form']/div[1]/div[2]/input[1]")).click();
    }
}
