package nopcommerceadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import nopcommerceadmin.Utils.*;

/**
 * Created by Sasikala on 05/03/2015.
 * This is an abstract class containing the Main Header wrapper for the site
 * All other pages implement this class in order to navigate to the other pages
 */
public abstract class Abstract_Header_Page extends Abstract_Page{

    public void goto_Dashboard_Page(){}

    public void  goto_Manage_Product_Page(){
        // mouseover for Catalog menu
        Actions catalog_menu = new Actions(driver) ;
        WebElement cm = driver.findElement(By.xpath("//*[@id='admin-menu']/li[2]/span"));
        catalog_menu.moveToElement(cm).build().perform();

        // mouseover for Products submenu
        WebElement product = driver.findElement(By.xpath("//*[@id='admin-menu']/li[2]/div/ul/li[3]/span"));
        catalog_menu.moveToElement(product).click().build().perform();

        // mouseover and click() for Manage products submenu
        WebElement manage_product = driver.findElement(By.xpath(".//*[@id='admin-menu']/li[2]/div/ul/li[3]/div/ul/li[1]/a"));
        catalog_menu.moveToElement(manage_product).click().build().perform();

        Wait_Utils.wait_For_Element_Displayed(By.linkText("Add new"));

    }

    public void goto_Sales_Page(){}

    public void goto_Customers_Page(){}

    public void goto_Promotions_Page(){}

    public void goto_Content_Management_Page(){}

    public void goto_Configuration_Page(){}

    public void goto_System_Page(){}

    public void goto_Help_Page(){}

    public void clear_cache(){}

    public void logout(){}

}
