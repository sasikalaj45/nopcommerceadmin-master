package nopcommerceadmin;

import org.openqa.selenium.By;
/**
 * Created by Sasikala on 05/03/2015.
 * Login page of the nopcommerce demo site
 */

public class Login_Page extends Abstract_Page {

    public void login(String emailid, String password){
        driver.findElements(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys(emailid);
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys(password);
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
    }
}
