package nopcommerceadmin;

import nopcommerceadmin.Utils.Wait_Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.awt.*;

/**
 * Created by Sasikala on 05/03/2015.
 * Additional attributes for the product can be added through the product edit page.
 * - Add SEO metadata
 * - Set Related and recommended products
 * - Set Product tags
 * - Upload Image for the product
 * - Edit admin data
 */
public class Edit_Products_Page extends Add_Product_Page {

    public void goto_SEO_Page() throws InterruptedException ,AWTException {

        Thread.sleep(2000);

        WebDriverWait wait = new WebDriverWait(driver, 100);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath
                ("//a[contains(text(),'SEO')]")));

        driver.findElement(By.xpath("//a[contains(text(),'SEO')]")).click();

        String Xpath = "//*[@id='product-edit-2']/table/tbody/tr[1]/td[1]/label[contains(text(), 'Meta keywords')]";

        Wait_Utils.is_Element_Present(driver.findElement(By.xpath(Xpath)));
    }

    public void edit_Admin_Content(String admin_data) {
        driver.findElement(By.id("AdminComment")).sendKeys(admin_data);
    }

    public  void save_edit_product(){
        driver.findElement(By.xpath(".//*[@id='product-form']/div[1]/div[2]/input[1]")).click();
    }

    public void goto_Picture_Page() throws InterruptedException ,AWTException {

        Thread.sleep(2000);

        WebDriverWait wait = new WebDriverWait(driver,100);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath
                ("//a[contains(text(),'Pictures')]")));

        driver.findElement(By.xpath("//a[contains(text(),'Pictures')]")).click();

    }
}
