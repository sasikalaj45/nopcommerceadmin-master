package nopcommerceadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import nopcommerceadmin.Utils.*;

/**
 * Created by Sasikala on 05/03/2015.
 * Manage product page provide functions such as
 * - link to add new product
 * - go to a product page
 * - import from Excel page
 * - search for a product
 * - select a product to edit from the list
 * - verify whether the product is present or not
 * - select a product and delete
 */
public class Manage_Product_Page extends Home_Page {

    public void goto_Add_NewProduct_Page() throws InterruptedException {

        driver.findElement(By.linkText("Add new")).click();
        Wait_Utils.wait_For_Element_Displayed(By.linkText("Product Info"));
    }

    public void goto_Sku_Page(String Sku) throws InterruptedException {

        driver.findElement(By.id("GoDirectlyToSku")).clear();
        driver.findElement(By.id("GoDirectlyToSku")).sendKeys(Sku);
        driver.findElement(By.id("go-to-product-by-sku")).click();
        Wait_Utils.wait_For_Element_Displayed(By.linkText("Product Info"));
    }

    public void goto_Import_from_excel_page() {

        driver.findElement(By.id("importexcel")).click();
    }

    public void search_product(String search_product_name) throws InterruptedException
    {
        driver.findElement(By.id("SearchProductName")).clear();
        driver.findElement(By.id("SearchProductName")).sendKeys(search_product_name);
        driver.findElement(By.id("search-products")).click();

    /**    if (Alert_Utils.is_Alert_Present()) {
            Alert_Utils.close_Alert_And_Get_Its_Text();
        }
*/
        Wait_Utils.pause(5);
    }

    public boolean is_product_present(String search_product_name ){

        String Xpath = "(//div[@id='products-grid']/table/tbody/tr/td[3])[1][contains(text(), '" + search_product_name + "')]";
        return Wait_Utils.is_Element_Present(driver.findElement(By.xpath(Xpath)));
    }

    public void select_a_product_to_edit(String select_product_name)
    {

        String product_path = "(//td[contains(text(),\"" + select_product_name + "\")]/following-sibling::td[6]/a)[1]";

        List<WebElement> aelement = driver.findElements(By.xpath(product_path));

    // There were many elements with the same description. Select the first product to edit by click the edit link

        for ( WebElement a : aelement )
         {
            System.out.println(a.getText());
            a.click();
            break;
         }

        Wait_Utils.wait_For_Element_Displayed(By.linkText("Product Info"));
    }

    public void select_a_product(String search_product_name) {

        String product_path = "(//td[contains(text(), '" +search_product_name +"')]/preceding-sibling::td[2]/input)[1]";

        // There were many elements with the same description. Select the first product by clicking the check box

        List<WebElement> aelement = driver.findElements(By.xpath(product_path));

        for (WebElement a : aelement) {
            a.click();
            break;
         }

   }

    public void delete_a_product()
    {
        driver.findElement(By.id("delete-selected")).click();
    }
}
